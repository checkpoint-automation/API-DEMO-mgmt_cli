#!/bin/bash
# clear the screen for clearity 
clear

echo "Set R80.10 API to accept all ip addresses"
mgmt_cli -r true set api-settings accepted-api-calls-from "All IP addresses" --domain 'System Data'

echo "Add user api_user with password vpn123"
mgmt_cli -r true add administrator name "api_user" password "vpn123" must-change-password false authentication-method "INTERNAL_PASSWORD" permissions-profile "Super User" --domain 'System Data'

echo "Restarting API Server"
api restart

echo "API Status"
api status

# mgmt_cli-demos

Some simple demonstration scripts

# Setup:
curl_cli https://gitlab.com/checkpoint-automation/API-DEMO-mgmt_cli/-/archive/master/API-DEMO-mgmt_cli-master.tar -k | tar xvf - ;\
cd API-DEMO-mgmt_cli-master/ ;\
find . -name "*.sh" -type f -exec chmod +x {} \;

./01_api_setup.sh


# BUGs:


# improve
- create better account for automation
- better hosts.csv
- better networks.csv
